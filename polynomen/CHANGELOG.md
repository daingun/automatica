# Changelog

## [1.1.0] - 2023-07-01
## Changed
- The eigenvalue calculation for root finding has been substituted with a custom one
- Removed `nalgebra` dependency

## [1.0.0] - 2022-07-17
## Added
- Greatest common divisor
- Polynomial norms
## Changed
- Split package from `automatica`
- Oldest supported rustc version has been increased to 1.56
### API Changes
- None
## Fixed
- None

# Changelog history from `automatica` crate

## [0.10.1] - 2021-04-18
## Added
- Benchmark polynomial multiplication
## Fixed
- Corrected fast Fourier transform algorithm

## [0.10.0] - 2021-03-07
## Added
- Polynomial exponentiation
## Changed
- Updated dependencies to latest available versions
- Oldest supported rustc version has been increased to 1.44
- Changed random testing library from quickcheck to proptest
### API Changes
- External dependencies are now re-exported from this library
## Fixed
- Improved the formatting of polynomials

## [0.9.0] - 2020-12-05
## Added
- Evaluation of polynomial ratios that avoids overflows

## [0.8.0] - 2020-08-30
## Added
- Documentation of the specification of the library
- Evaluation of polynomial now take reference values too
- Add polynomial creation from iterators
- AsRef trait to polynomial for reference-to-reference conversion into a slice
- Polynomial round off to zero
## Changed
- Improvements on polynomial roots finders and increment of related tests
- Polynomial numeric type no longer requires Copy trait
## Fixed
- Subtraction between a real number and a polynomial
- Derivation of zero degree polynomial
- Derivation of zero degree polynomials

## [0.7.0] - 2020-02-08
## Added
- Polynomial division
- Polynomial root finding using iterative Aberth-Ehrlich method.
- Polynomial multiplication using fast fourier transform

## [0.6.0] - 2019-11-18
## Added
- Polynomial derivation and integration
## Changed
- Generalization of polynomials
- The degree of a polynomial now returns an Option, which is None for zero polynomial
- Companion matrix is None for zero degree polynomial
