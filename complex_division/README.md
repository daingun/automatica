# Library for the calculation of complex numbers division.

[Repository](https://gitlab.com/daingun/automatica)

[Crate registry](https://crates.io/crates/complex_division)

[Documentation](https://docs.rs/complex_division)

This library implements the division between complex numbers with floating
point representation.

The algorithm is aimed to avoid overflows and underflows.

A dedicated method is available for complex number inversion.
It is guaranteed that:

```
compdiv(1., 0., c, d) = compinv(c, d)
```

## Minimum compiler version

The minimum `rustc` compiler version is 1.56.

## References

Michael Baudin, Robert L. Smith, A Robust Complex Division in Scilab, 2012, arXiv:1210.4539v2 [cs.MS]
