# On development

1. Ensure the correct library version is set (using `make version`);
2. Ensure the html documentation is built (using `make html`);
3. Ensure the changelog is up to date, with release date.

# On master

1. Merge without fast forward development branch;
2. Make the annotated tag (`git tag -a`) with the release number `\d+\.\d+\.\d+`;
3. Push to gitlab with tags (`git push --follow-tags`).

# On gitlab

1. Run manual task on gitlab continuous integration to publish the documentation and the crates;
2. Create a new release with latest changelog information from the tag and milestone. (Currently not applicable)
