//! Module to compute the greatest common divisor of two polynomials
use std::ops::{Add, Div, Mul, Neg, Sub};

use crate::{Abs, Epsilon, Inv, Max, One, Poly, Zero};

impl<T> Poly<T>
where
    T: Abs
        + Clone
        + Div<Output = T>
        + Epsilon
        + Inv
        + Max
        + Mul<Output = T>
        + One
        + PartialOrd
        + Sub<Output = T>
        + Zero,
{
    /// Greatest common divisor between `self` and `other`.
    /// Loss of precision may lead to wrong theoretical results.
    ///
    /// # Arguments
    ///
    /// * `other` - other polynomial
    ///
    /// # Example
    ///
    /// ```
    /// use polynomen::poly;
    /// let a = poly![-6.0_f32, -1., 1.];
    /// let b = poly![6., 5., 1.];
    /// let expected = poly![2., 1.];
    /// let actual = a.gcd(&b).monic().0;
    /// assert_eq!(expected, actual);
    /// ```
    #[must_use]
    pub fn gcd(&self, other: &Self) -> Self {
        if self.degree() < other.degree() {
            Self::gcd_int(other, self)
        } else {
            Self::gcd_int(self, other)
        }
    }

    /// Internal greatest common divisor algorithm.
    /// The degree of `a` must be greater than or equal to the degree of `b`.
    fn gcd_int(a: &Self, b: &Self) -> Self {
        if b.degree() <= Some(0) {
            return a.clone();
        }
        let mut rem = a % b;
        let max = b.norm_inf();
        rem.roundoff_mut(&(T::epsilon() * max));
        debug_assert!(rem.degree() < b.degree());
        Self::gcd_int(b, &rem)
    }
}

impl<T> Poly<T>
where
    T: Abs
        + Add<Output = T>
        + Clone
        + Div<Output = T>
        + Epsilon
        + Inv
        + Mul<Output = T>
        + One
        + Max
        + Neg<Output = T>
        + PartialEq
        + PartialOrd
        + Sub<Output = T>
        + Zero,
{
    /// Extended greatest common divisor algorithm between `self` and `other`.
    ///
    /// Given polynomials `a` and `b` this method return the tuple
    /// `(g, c, d)`, where `g` is the GCD and `c`, `d` are such that
    /// `a=g*c` and `b=g*d`
    ///
    /// # Arguments
    ///
    /// * `other` - other polynomial
    ///
    /// # Example
    ///
    /// ```
    /// use polynomen::poly;
    /// let a = poly![-6.0_f32, -1., 1.];
    /// let b = poly![6., 5., 1.];
    /// let expected = poly![2., 1.];
    /// let actual = a.gcd_ext(&b).0.monic().0;
    /// assert_eq!(expected, actual);
    /// ```
    ///
    /// # References
    ///
    /// <https://en.wikipedia.org/wiki/Polynomial_greatest_common_divisor#B%C3%A9zout's_identity_and_extended_GCD_algorithm>
    #[must_use]
    pub fn gcd_ext(&self, other: &Self) -> (Self, Self, Self) {
        if self.degree() < other.degree() {
            Self::gcd_ext_int(other, self)
        } else {
            Self::gcd_ext_int(self, other)
        }
    }

    /// Internal extended greatest common divisor algorithm.
    /// The degree of `a` must be greater than or equal to the degree of `b`.
    #[allow(clippy::many_single_char_names)]
    fn gcd_ext_int(a: &Self, b: &Self) -> (Self, Self, Self) {
        // Create simple two elements ring buffers.
        // While the new value is pushed on the back of the ring, the oldest
        // is popped from the front.
        use std::collections::VecDeque;
        let mut r = VecDeque::from([a.clone(), b.clone()]);
        let mut s = VecDeque::from([Poly::<T>::one(), Poly::<T>::zero()]);
        let mut t = VecDeque::from([Poly::<T>::zero(), Poly::<T>::one()]);

        let mut i = false;
        while !r[1].is_zero() {
            i = !i;
            let q = &r[0] / &r[1];

            r.push_back(&r[0] - &(&q * &r[1]));
            r.pop_front();

            let max = r[0].norm_inf();
            r[1].roundoff_mut(&(T::epsilon() * max));
            // Here r[0] cannot be the zero polynomial (degree=None)
            let new_degree = r[0].degree().unwrap_or(0) - 1;
            r[1].truncate(new_degree);

            // Always deg(r_1) < deg(r_0) with a % r_0 = r_1
            debug_assert!(r[1].degree() < r[0].degree());

            t.push_back(t[0].clone() - &q * &t[1]);
            t.pop_front();
            s.push_back(s[0].clone() - &q * &s[1]);
            s.pop_front();
        }
        let a1 = if i { -t[1].clone() } else { t[1].clone() };
        let b1 = if i { s[1].clone() } else { -s[1].clone() };
        (r[0].clone(), a1, b1)
    }
}

#[cfg(test)]
mod tests {
    use crate::poly;

    #[test]
    #[should_panic]
    fn poly_gcd1() {
        let p1 = poly!(14., -3., 4., -4., 1.);
        let p2 = poly!(6., 17., 12., 8., 1.);
        let res = p1.gcd(&p2).monic().0;
        assert_eq!(poly!(2., 1., 1.), res);
    }

    #[test]
    fn poly_gcd2() {
        let p1 = poly!(6., 7., 1.);
        let p2 = poly!(-6., -5., 1.);
        let res = p1.gcd(&p2);
        assert_eq!(poly!(12., 12.), res);
    }

    #[test]
    fn poly_gcd3() {
        let p1 = poly!(1., 1.);
        let p2 = poly!(-1., 0., 1.);
        let res = p1.gcd(&p2);
        assert_eq!(p1, res);
    }

    #[test]
    fn poly_gcd_ext1() {
        let p1 = poly!(14., -3., 4., -4., 1.);
        let p2 = poly!(6., 17., 12., 8., 1.);
        let res = p1.gcd_ext(&p2);
        assert_eq!(poly!(2., 1., 1.), res.0.monic().0);
    }

    #[test]
    fn poly_gcd_ext2() {
        let p1 = poly!(6., 7., 1.);
        let p2 = poly!(-6., -5., 1.);
        let res = p1.gcd_ext(&p2);
        assert_eq!(poly!(12., 12.), res.0);
        assert_eq!(poly!(1. / 2., 1. / 12.), res.1);
        assert_eq!(poly!(-1. / 2., 1. / 12.), res.2);
    }

    #[test]
    fn poly_gcd_ext3() {
        let p1 = poly!(1., 1., 1., 1.);
        let p2 = poly!(1., 0., 0., 1.);
        let res = p1.gcd_ext(&p2);
        assert_eq!(poly!(1., 1.), res.0);
        assert_eq!(poly!(1., 0., 1.), res.1);
        assert_eq!(poly!(1., -1., 1.), res.2);
    }

    #[test]
    fn poly_gcd_ext4() {
        let p1 = poly!(-1., 1.);
        let p2 = poly!(-1., 0., 1.);
        let res = p1.gcd_ext(&p2);
        assert_eq!(p1, res.0);
    }
}
